FROM nginx:alpine
LABEL author="Lakshmi Sabbapathy"
COPY ./dist/course /usr/share/nginx/html
EXPOSE 80 443
ENTRYPOINT ["nginx","-g","daemon off;"]
